$( document ).ready(function() {
    var width = $('.wrap-nav').width();
    console.log(width);

    var scrollLeftAmount =100;
    $("#btn-next-bread").click(function () {
        $('.breadcrumb-group').animate({
            scrollLeft:'+='+scrollLeftAmount
        },500);
    });
    var checkScroll = $('.breadcrumb-group').hasScrollBar();
    if (checkScroll == true){
        $("#btn-next-bread").addClass("active");
    }
    else{
        $("#btn-next-bread").removeClass("active");
    }

});
(function($) {
    $.fn.hasScrollBar = function() {
        return this.get(0).scrollWidth > this.width();
    }
})(jQuery);